﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MaterialController : ControllerBase
    {
        private readonly ApplicationContext _context;
        private readonly ILogger<MaterialController> _logger;
        private readonly string _filesPath;

        public MaterialController(ApplicationContext context, ILogger<MaterialController> logger)
        {
            _context = context;
            _filesPath = Directory.GetCurrentDirectory() + "\\";
            _logger = logger;
        }

        // GET: api/Material
        [HttpGet]
        public IActionResult GetMaterials(Category? category)
        {
            _logger.LogInformation("Downloading materials of " + category + "category.");
            if (category != null)
                return Ok(_context.Materials.Include(p => p.Versions).Where(p => p.Category == category).ToList());

            return Ok(_context.Materials.Include(p => p.Versions).ToList());
        }

        // GET: api/Material/{id}
        [HttpGet("{id}")]
        public IActionResult GetMaterialById(int id)
        {
            _logger.LogInformation("Downloading material with id = " + id);
            var material = _context.Materials.AsNoTracking()
                .Include(p => p.Versions)
                .FirstOrDefault(p => p.Id == id);
            if (material == null)
                return NotFound();

            return Ok(material);
        }

        // GET: api/Material/{name}
        [HttpGet("{name}")]
        public IActionResult GetMaterialByName(string name)
        {
            _logger.LogInformation("Downloading  material with name = " + name);
            var material = _context.Materials.AsNoTracking()
                .Include(p => p.Versions)
                .FirstOrDefault(p => p.Name == name);
            if (material == null)
                return NotFound();

            return Ok(material);
        }

        [HttpPatch]
        [Authorize(Roles = "admin")]
        public IActionResult ChangeMaterialCategory(string name, Category category)
        {
            _logger.LogInformation("Change category of material with name = " + name + " on " + category);
            var material = _context.Materials.FirstOrDefault(p => p.Name == name);

            if (material == null)
                return NotFound();
            if ((int)category < 0 || (int)category > 2)
                return BadRequest("Invalid category");
            material.Category = category;
            _context.SaveChanges();
            return Ok(material);
        }

        // POST: api/Material
        [HttpPost]
        public async Task<ActionResult<Material>> CreateMaterial(MaterialDTO model)
        {
            _logger.LogInformation("Create material.");
            var materialPath = $"{_filesPath}{model.Name}\\";

            if (_context.Materials.Any(m => m.Name == model.Name))
                return (BadRequest($"Material with name {model.Name} already exists"));

            if (!ModelState.IsValid) return BadRequest("Invalid model");
            if ((int)model.Category > 2 || (int)model.Category < 0)
                return BadRequest("Invalid category");
            Directory.CreateDirectory(materialPath);
            await System.IO.File.WriteAllBytesAsync($"{materialPath}{model.Name}_1", model.File);

            var material = new Material { Name = model.Name, Category = model.Category };
            await _context.Materials.AddAsync(material);
            var version = new Models.Version
            {
                Material = material,
                UploadDateTime = DateTime.Now,
                Release = 1,
                Size = model.File.Length,
                Path = $"{materialPath}{material.Name}_1"
            };
            await _context.Versions.AddAsync(version);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetMaterial", new { id = material.Id }, material);
        }


        [Route("update")]
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<Material>> UpdateMaterial(VersionDTO model)
        {
            _logger.LogInformation("Update material.");
            if (ModelState.IsValid)
            {
                Material material = _context.Materials.Include(p => p.Versions).FirstOrDefault(p => p.Name == model.Name);
                if (material == null)
                    return NoContent();
                var materialPath = $"{_filesPath}{model.Name}\\";
                var release = material.Versions.Count + 1;
                var version = new Models.Version()
                {
                    MaterialId = material.Id,
                    Material = material,
                    Release = release,
                    UploadDateTime = DateTime.Now,
                    Size = model.File.Length,
                    Path = $"{materialPath}{model.Name}_{release}"
                };
                _context.Versions.Add(version);
                Directory.CreateDirectory(materialPath);
                System.IO.File.WriteAllBytes($"{materialPath}{model.Name}_{release}", model.File);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetMaterial", new { id = material.Id }, material);
            }
            return BadRequest("Invalid model");
        }


        [HttpGet("download/{name}")]
        public IActionResult DownloadMaterial(int id, int? version_num)
        {
            var material = _context.Materials.Include(v => v.Versions).FirstOrDefault(p => p.Id == id);
            if (material == null)
                return NoContent();
            Models.Version version;
            if (version_num != null)
                version = material.Versions.FirstOrDefault(v => v.Release == version_num);
            else
                version = material.Versions.Last();
            _logger.LogInformation("Download material with Id = " + id + " and version = " + version);
            string fileName = $"{material.Name}_{version.Release}";
            string path = Path.Combine(_filesPath, material.Name, fileName);
            byte[] file = System.IO.File.ReadAllBytes(path);
            return File(file, "application/octet-stream", fileName);
        }
        
    }
}
